import 'package:flutter/material.dart';
import 'package:flutter_components/models/models.dart';
import 'package:flutter_components/screens/screens.dart';

class AppRoutes {
  static const initialRoute = 'home';
  static final menuOptions = <MenuOption>[
    //  MenuOption(
    // route: 'home',
    // icon: Icons.home,
    //   name: 'Home Screen',
    //     screen: const HomeScreen()),
    MenuOption(
        route: 'listView1',
        icon: Icons.home,
        name: 'List View tipo 1',
        screen: const ListView1Screen()),
    MenuOption(
        route: 'listView2',
        icon: Icons.home,
        name: 'List View tipo 2',
        screen: const ListView2Screen()),
    MenuOption(
        route: 'alert',
        icon: Icons.home,
        name: 'Alert',
        screen: const AlertScreen()),
    MenuOption(
        route: 'card',
        icon: Icons.home,
        name: 'Card',
        screen: const CardScreen()),
    MenuOption(
        route: 'avatar',
        icon: Icons.access_alarm_outlined,
        name: 'Avatar',
        screen: const AvatarScreen()),
    MenuOption(
        route: 'animated',
        icon: Icons.access_alarm_outlined,
        name: 'Animated',
        screen: const AnimatedScreen()),
    MenuOption(
        route: 'inputs',
        icon: Icons.access_alarm_outlined,
        name: 'inputs',
        screen: const InputsScreen()),
    MenuOption(
        route: 'slider',
        icon: Icons.access_alarm_outlined,
        name: 'Slider',
        screen: const SliderScreen()),
    MenuOption(
        route: 'listviewbuilder',
        icon: Icons.account_circle_outlined,
        name: 'InfiniteScroll',
        screen: const ListViewBuilderScreen()),
  ];
  static Map<String, Widget Function(BuildContext)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};
    appRoutes.addAll({'home': (BuildContext contex) => HomeScreen()});
    for (final option in menuOptions) {
      appRoutes.addAll({option.route: (BuildContext contex) => option.screen});
    }

    return appRoutes;
  }

  // static Map<String, Widget Function(BuildContext)> routes = {
  //   'home': (BuildContext context) => const HomeScreen(),
  // 'listView1': (BuildContext context) => const ListView1Screen(),
  //'listView2': (BuildContext context) => const ListView2Screen(),
  //'alert': (BuildContext context) => const AlertScreen(),
  //'card': (BuildContext context) => const CardScreen(),
  //};
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => const AlertScreen(),
    );
  }
}
