import 'package:flutter/material.dart';
import 'package:flutter_components/widgets/widgets.dart';

class CardScreen extends StatelessWidget {
  const CardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Card Widget'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        children: const[
          CustomCardType1(),
          SizedBox(height: 10),
          CustomCardType2(imageUrl: 'https://static.vecteezy.com/system/resources/previews/003/031/484/non_2x/10-years-anniversary-banner-template-vector.jpg', name: 'Eres mi vida entera',),  
          SizedBox(height: 20),
          CustomCardType2(imageUrl: 'https://i.musicaimg.com/letras/max/1148959.jpg', name: 'Hola',),
          SizedBox(height: 20),
          CustomCardType2(imageUrl: 'http://descubrenombres.com/wp-content/uploads/2014/09/miguel-significado.jpg', name: null,),
          SizedBox(height: 100),
        ],
      ),
    );
  }
}


