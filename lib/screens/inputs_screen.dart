import 'package:flutter/material.dart';
import 'package:flutter_components/widgets/widgets.dart';

class InputsScreen extends StatelessWidget {
  const InputsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> myFormKey = GlobalKey<FormState>();
    final Map<String, String> formValues = {
      'first_name': 'Ericka',
      'last_name': 'GUanoluisa',
      'email': 'ericka@hotmail.com',
      'password': '123456',
      'role': 'Admin'
    };
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Form(
          key: myFormKey,
          child: Column(
            children: [
              CustomInputField(
                labelText: 'Nombre',
                hintText: 'Nombre....ssss',
                formValues: formValues,
                formProperty: 'first_name',
              ),
              SizedBox(height: 30),
              CustomInputField(
                  labelText: 'Apellido',
                  hintText: 'Apellido...key',
                  formProperty: 'last_name',
                  formValues: formValues),
              SizedBox(height: 30),
              CustomInputField(
                  labelText: 'Correo',
                  hintText: 'Corre ...',
                  keyboardType: TextInputType.emailAddress,
                  formProperty: 'email',
                  formValues: formValues),
              SizedBox(height: 30),
              CustomInputField(
                labelText: 'Coontraseña',
                hintText: 'Contraseña ...',
                obscureText: true,
                formValues: formValues,
                formProperty: 'password',
              ),
              SizedBox(height: 30),
              DropdownButtonFormField<String>(
                value: 'Admin',
                items: const [
                  DropdownMenuItem(value: 'Admin', child: Text('Admin')),
                  DropdownMenuItem(value: 'SuperUsr', child: Text('SuperUsr')),
                  DropdownMenuItem(value: 'Developer', child: Text('Developer')),
                  DropdownMenuItem(value: 'Jr Developer', child: Text('Jr Developer')),
                ],
                onChanged: (value) {
                  print(value);
                  formValues['role'] = value ?? 'Admin';
                },
              ),
              ElevatedButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  if (!myFormKey.currentState!.validate()) {
                    print('Formualio no valido');
                    return;
                  }
                  print(formValues);
                },
                child: const SizedBox(
                    width: double.infinity,
                    child: Center(child: const Text('Guardar'))),
              )
            ],
          ),
        ),
      ),
    );
  }
}
