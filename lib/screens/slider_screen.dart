import 'package:flutter/material.dart';
import 'package:flutter_components/theme/app_theme.dart';

class SliderScreen extends StatefulWidget {
  const SliderScreen({Key? key}) : super(key: key);

  @override
  _SliderScreenState createState() => _SliderScreenState();
}

class _SliderScreenState extends State<SliderScreen> {
  double _slidervalue = 100;
  bool _sliderEnable = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Slider '),
        ),
        body: Column(
          children: [
            Slider.adaptive(
                min: 50,
                max: 400,
                activeColor: AppTheme.primary,
                value: _slidervalue,
                onChanged: _sliderEnable
                    ? (value) {
                        _slidervalue = value;
                        setState(() {});
                      }
                    : null),
            Checkbox(
                value: _sliderEnable,
                onChanged: (value) {
                  _sliderEnable = value ?? true;
                  setState(() {});
                }),
            CheckboxListTile(
                activeColor: AppTheme.primary,
                title: const Text('Habilitar '),
                value: _sliderEnable,
                onChanged: (value) => setState(() {
                      _sliderEnable = value ?? true;
                    })),
            Switch(
                value: _sliderEnable,
                onChanged: (value) => setState(() {
                      _sliderEnable = value;
                    })),
            SwitchListTile.adaptive(
                activeColor: AppTheme.primary,
                title: const Text('Habilitar '),
                value: _sliderEnable,
                onChanged: (value) => setState(() {
                      _sliderEnable = value;
                    })),
            const AboutListTile(),
            Expanded(
              child: Image(
                image: NetworkImage(
                    'https://w7.pngwing.com/pngs/647/435/png-transparent-patrick-star-desktop-high-definition-television-1080p-spongebob-television-food-heroes.png'),
                fit: BoxFit.contain,
                width: _slidervalue,
              ),
            )
          ],
        ));
  }
}
