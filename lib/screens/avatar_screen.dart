import 'package:flutter/material.dart';

class AvatarScreen extends StatelessWidget {
  const AvatarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Vladimir Putin')),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 5),
            child: CircleAvatar(
              child: const Text('VP'),
              backgroundColor: Colors.indigo[900],
            ),
          )
        ],
      ),
      body: Center(
        child: CircleAvatar(
          radius: 250,
          backgroundImage: NetworkImage(
            'https://as01.epimg.net/diarioas/imagenes/2022/02/25/actualidad/1645793297_165735_1645803750_noticia_normal.jpg',
          ),
        ),
      ),
    );
  }
}
